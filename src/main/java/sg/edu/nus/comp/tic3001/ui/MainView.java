package sg.edu.nus.comp.tic3001.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import sg.edu.nus.comp.tic3001.model.FileObj;
import sg.edu.nus.comp.tic3001.ui.UiController.KwicUi;

public class MainView extends JFrame implements KwicUi {

	private static final long serialVersionUID = -3445311782196514706L;

	private JTextArea linesInput;
	private JTextArea ignoreWordsInput;
	private JTextArea requiredWordsInput;
	private JTextArea resultsOutput;
	private JTextArea inputresultsOutput;
	private JTextArea timeresultsOutput;
	private JButton generateButton;
	private JButton clearAllButton;
	private JButton exportResultButton;
	private FileObj fileobjresult;
	
	private UiController controller;
	
	public MainView() {
		super("Key Word In Context");
		add(createAndAddComponents());
		attachButtonEvents();
		pack();
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private JPanel createAndAddComponents() {
		JPanel mainPanel = new JPanel(new GridLayout(0, 3));

		// Left Panel
		JPanel userFileInputPanel = new JPanel(new GridLayout(1, 0));
		userFileInputPanel.setPreferredSize(new Dimension(400, 480));
		JPanel inputresultPanel = new JPanel();

		// Middle Panel
		JPanel userInputPanel = new JPanel(new GridLayout(4, 0));
		userInputPanel.setPreferredSize(new Dimension(400, 480));
		JPanel linesInputPanel = new JPanel();
		JPanel ignoreWordsInputPanel = new JPanel();
		JPanel requiredWordsInputPanel = new JPanel();
		
		// Right Panel
		JPanel rightPanel = new JPanel(new GridBagLayout());
		rightPanel.setPreferredSize(new Dimension(400, 480));
		JPanel timePanel = new JPanel();
		JPanel resultPanel = new JPanel();
		JPanel architectureSelectionPanel = new JPanel();
		JPanel operationPanel = new JPanel();

		// Input Results output
		inputresultPanel.setBorder(new TitledBorder(new EtchedBorder(), "File Input Preview"));
		inputresultsOutput = new JTextArea(25, 30);
		inputresultsOutput.setEditable(false);
		JScrollPane inputFileDisplayScroll = new JScrollPane(inputresultsOutput);
		inputFileDisplayScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		inputresultPanel.add(inputFileDisplayScroll);

		// Input Results output
		timePanel.setBorder(new TitledBorder(new EtchedBorder(), "Total execution time:"));
		timeresultsOutput = new JTextArea(3,30);
		timeresultsOutput.setEditable(false);
		JScrollPane timeDisplayScroll = new JScrollPane(timeresultsOutput);
		timeDisplayScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		timePanel.add(timeDisplayScroll);

		// Lines input
		linesInputPanel.setBorder(new TitledBorder(new EtchedBorder(), "Lines Input"));
		linesInput = new JTextArea(5, 30);
		linesInput.setEditable(true);
		JScrollPane linesInputScroll = new JScrollPane(linesInput);
		linesInputScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		linesInputPanel.add(linesInputScroll);

		// Ignore words input
		ignoreWordsInputPanel.setBorder(new TitledBorder(new EtchedBorder(), "Words Ignored"));
		ignoreWordsInput = new JTextArea(5, 30);
		ignoreWordsInput.setEditable(true);
		JScrollPane ignoreWordsInputScroll = new JScrollPane(ignoreWordsInput);
		ignoreWordsInputScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		ignoreWordsInputPanel.add(ignoreWordsInputScroll);
		
		// Required words input
		requiredWordsInputPanel.setBorder(new TitledBorder(new EtchedBorder(), "Words Required"));
		requiredWordsInput = new JTextArea(5, 30);
		requiredWordsInput.setEditable(true);
		JScrollPane requiredWordsInputScroll = new JScrollPane(requiredWordsInput);
		requiredWordsInputScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		requiredWordsInputPanel.add(requiredWordsInputScroll);

		userFileInputPanel.add(inputresultPanel);
		userInputPanel.add(linesInputPanel);
		userInputPanel.add(ignoreWordsInputPanel);
		userInputPanel.add(requiredWordsInputPanel);



		// Results output
		resultPanel.setBorder(new TitledBorder(new EtchedBorder(), "Results"));
		resultsOutput = new JTextArea(15, 30);
		resultsOutput.setEditable(false);
		JScrollPane outputDisplayScroll = new JScrollPane(resultsOutput);
		outputDisplayScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		resultPanel.add(outputDisplayScroll);
		
		// Operation area
		generateButton = new JButton("Generate");
		clearAllButton = new JButton("Clear All");
		exportResultButton = new JButton("Export");

		operationPanel.setLayout(new BoxLayout(operationPanel, BoxLayout.X_AXIS));
		operationPanel.add(Box.createHorizontalGlue());
		operationPanel.add(generateButton);
		operationPanel.add(clearAllButton);
		operationPanel.add(exportResultButton);
		operationPanel.add(Box.createHorizontalGlue());
		
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.PAGE_START;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		c.weighty = 0.8;
		rightPanel.add(resultPanel, c);
		c.anchor = GridBagConstraints.LAST_LINE_START;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.weightx = 1.0;
		c.weighty = 0.1;
		rightPanel.add(architectureSelectionPanel, c);
		c.anchor = GridBagConstraints.LAST_LINE_START;
		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1.0;
		c.weighty = 0.1;
		rightPanel.add(timePanel, c);
		c.gridx = 0;
		c.gridy = 3;
		c.weightx = 1.0;
		c.weighty = 0.1;
		rightPanel.add(operationPanel, c);

		mainPanel.add(userFileInputPanel);
		mainPanel.add(userInputPanel);
		mainPanel.add(rightPanel);

		return mainPanel;
	}
	
	private void attachButtonEvents() {
		generateButton.addActionListener(e -> controller.generateResult());

		clearAllButton.addActionListener(e -> {
			linesInput.setText("");
			ignoreWordsInput.setText("");
			resultsOutput.setText("");
			requiredWordsInput.setText("");
		});
		
		exportResultButton.addActionListener(e -> {
			controller.exportResultToFile(resultsOutput.getText());
			JOptionPane.showMessageDialog(null, "Data exported to output.txt");
		});
	}

	@Override
	public List<String> getInput() {
		List<String> linesList = Arrays.asList(getInputArray());
		return linesList;
	}

	@Override
	public Set<String> getIgnoredWords() {
		String ignoreWords = ignoreWordsInput.getText();
		String[] ignoreWordsList = ignoreWords.split("\n");
		Set<String> ignoreWordsSet = new HashSet<>();
		for (String word : ignoreWordsList) {
			ignoreWordsSet.add(word);
		}
		return ignoreWordsSet;
	}
	
	@Override
	public Set<String> getRequiredWords() {
		String requiredWords = requiredWordsInput.getText();
		String[] requiredWordsList = requiredWords.split("\n");
		Set<String> requiredWordsSet = new HashSet<>();
		for (String word : requiredWordsList) {
			word = word.trim();
			if (!word.equals("")) requiredWordsSet.add(word);
		}
		return requiredWordsSet;
	}

	@Override
	public void setResults(List<String> results,String time) {
		if (results.isEmpty()) {
			resultsOutput.setText("");
			return;
		}
		StringBuilder builder = new StringBuilder();
		for (String entry : results) {
			if(entry.endsWith(".txt")||entry.equals("UserInput")){
				builder.append("-"+entry);
			}
			else{
				builder.append(entry);
			}
			builder.append("\n");
		}

		builder.setLength(builder.length() - 1); // remove the new line in the end
		timeresultsOutput.setText(time);
		System.out.println(time);
		resultsOutput.setText(builder.toString());
	}

	public JTextArea getInputresultsOutput() {
		return inputresultsOutput;
	}

	public JTextArea getTimeresultsOutput() {
		return timeresultsOutput;
	}

	@Override
	public void setInputResults(List<FileObj> results) {
		if (results.isEmpty()) {
			inputresultsOutput.setText("");
			return;
		}
		StringBuilder builder = new StringBuilder();
		for (FileObj entry : results) {
			builder.append(entry.getFilename());
			builder.append("\n");
			for (String i : entry.getContent()){
				builder.append(i);
				builder.append("\n");
			}
			builder.append("\n");
		}
		builder.setLength(builder.length() - 1); // remove the new line in the end
		inputresultsOutput.setText(builder.toString());
	}

	@Override
	public JTextArea getOutputTextArea() {
		return resultsOutput;
	}

	@Override
	public JTextArea getInputOutputTextArea() {
		return inputresultsOutput;
	}

	@Override
	public List<String> getFileInput() {
		List<String> linesList = Arrays.asList(getFileInputArray());
		return linesList;
	}

	@Override
	public void setController(UiController controller) {
		this.controller = controller;
	}

	public String[] getFileInputArray() {
		String inputLines = inputresultsOutput.getText();
		String[] lines = inputLines.split("\n");
		return lines;
	}
	@Override
	public String[] getInputArray() {
		String inputLines = linesInput.getText();
		String[] lines = inputLines.split("\n");
		return lines;
	}
}

