package sg.edu.nus.comp.tic3001.ui;

import java.io.*;
import java.util.*;

import javax.swing.JTextArea;
import javax.swing.plaf.synth.SynthOptionPaneUI;

import sg.edu.nus.comp.tic3001.control.MasterControl;
import sg.edu.nus.comp.tic3001.model.FileObj;

import static sg.edu.nus.comp.tic3001.model.FileObj.*;

public class UiController {
	List<FileObj> outputFileObj = new LinkedList<>();
	public interface KwicUi {
		/**
		 * Get the input String lists
		 */
		List<String> getInput();
		/**
		 * Get the input String lists
		 */
		List<String> getFileInput();


		/**
		 * Get the input strings as an String array
		 */
		String[] getInputArray();

		/**
		 * Get the output text area in the UI
		 */
		JTextArea getOutputTextArea();
		/**
		 * Get the output text area in the UI
		 */
		JTextArea getInputOutputTextArea();
		/**
		 * Get the "words to ignore" set
		 */
		Set<String> getIgnoredWords();

		/**
		 * Get the "required words" set
		 */
		Set<String> getRequiredWords();

		/**
		 * Set the generated KWIC results in the UI
		 */
		void setResults(List<String> results,String time);

		/**
		 * Set the generated KWIC results in the UI
		 * @param results
		 */
		void setInputResults(List<FileObj> results);

		/**
		 * Set the UI Controller
		 */
		void setController(UiController controller);

	}

	final private KwicUi view;

	private MasterControl controller;

	public UiController(KwicUi view) {
		this.view = view;
		controller = new MasterControl();
		File dir = new File("/Users/wanyinlim/Desktop/TIC3001/Assignment2_Solution-master/src/main/listoffiles");
		File[] files = dir.listFiles();
		List<String> inputWordSet = null;

		FileObj fileobjects = null;
		String line = "";
		// Fetching all the files
		Arrays.sort(files);
		for (File file : files) {
			if(file.isFile()) {
				BufferedReader inputStream = null;
				fileobjects = new FileObj();
				inputWordSet = new LinkedList<>();
				try {
					inputStream = new BufferedReader(new FileReader(file));
					fileobjects.setFilename(file.getName());
					while ((line = inputStream.readLine()) != null) {
							inputWordSet.add(line);
					}
					fileobjects.setContent(inputWordSet);

				}catch(IOException e) {
					System.out.println(e);
				}
				outputFileObj.add(fileobjects);
			}
		}
		view.setInputResults(outputFileObj);
	}

	public void generateResult() {
		// Get Start time
		long startTime = System.currentTimeMillis();
		// Get entered ignored words from GUI
		Set<String> ignoredWordsSet = view.getIgnoredWords();
		// Get entered required words from GUI
		Set<String> requiredWordsSet = view.getRequiredWords();
		// Run the application
		List<String> newList = new ArrayList<String>();
		List<FileObj> fileobjects = outputFileObj;
		for (FileObj obj:fileobjects){
			List<String> contentlist = controller.run(obj.getContent(), ignoredWordsSet, requiredWordsSet);
			if(contentlist.size()!=0){
				newList.add(obj.getFilename());
				newList.addAll(contentlist);
			}
		}
		List<String> result2 = controller.run(view.getInput(), ignoredWordsSet, requiredWordsSet);
			// Display result
		if(result2.size()!=0){
			newList.add("UserInput");
		}
		newList.addAll(result2);
		long endTime = System.currentTimeMillis();
		String timeTaken = (endTime - startTime) + " milliseconds";
		view.setResults(newList,timeTaken);

	}

	public void exportResultToFile(String data) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter("./output.txt"));
			view.getOutputTextArea().write(writer);
		} catch (IOException e) {
			System.err.println(e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					System.err.println(e);
				}
			}
		}
	}
}
