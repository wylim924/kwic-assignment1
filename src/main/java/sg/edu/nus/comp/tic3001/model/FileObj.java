package sg.edu.nus.comp.tic3001.model;

import java.util.List;

public class FileObj {
    private String filename;
    private List<String> content;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public List<String> getContent() {
        return content;
    }

    public void setContent(List<String> content) {
        this.content = content;
    }
}
