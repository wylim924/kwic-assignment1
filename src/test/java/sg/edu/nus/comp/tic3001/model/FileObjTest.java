package sg.edu.nus.comp.tic3001.model;

import static org.junit.Assert.assertEquals;

import java.util.Collections;

import org.junit.Test;

public class FileObjTest {
    @Test
    public void testSetContent() {
        FileObj line = new FileObj();
        line.setContent(Collections.singletonList("The Day after Tomorrow"));
        assertEquals(Collections.singletonList("The Day after Tomorrow"), line.getContent());
    }
}
